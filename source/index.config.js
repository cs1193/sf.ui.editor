/* @flow */

export default function IndexConfig($stateProvider) {
  $stateProvider
    .state('editor', {
      url: '/editor',
      template: '<div>Editor</div>',
    });
}
