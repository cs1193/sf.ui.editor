/* @flow */

import angular from 'angular';
import angularUIRouter from 'angular-ui-router';
import angularUIBootstrap from 'angular-ui-bootstrap';

import IndexConfig from './index.config';

export default angular.module('sf.ui.editor', [
  angularUIRouter,
  angularUIBootstrap,
])
  .config(IndexConfig)
  .name;
